//loading the expressjs module into our app and into the expressvariable.
//express module will allow us to use expressjs methods to create our api.
const express = require("express");

//create an application with expressjs
//This creates an express.js application and stores it as app
//app is our server
const app = express();

//port is a variable to contain the port we when we want to designate
const port = 4000;

//express.json() allows us to handle the request's body and automatically 
app.use(express.json());

let users = [
	
	{
		email: "mighty12@gmail.com",
		username: "mightMouse12",
		password:"notrelatedtomickey",
		isAdmin: false
	},
	{
		email: "minnieMouse@gmail.com",
		username: "minniexmickey",
		password:"minniesincethestart",
		isAdmin: false
	},
	{
		email: "mickeyTheMouse@gmail.com",
		username: "mickeyKing",
		password:"thefacethatrunstheplace",
		isAdmin: true
	},

]
let items = [
	{
		"name":"Banana",
		"price":10,
		"stock":5,
		"isActive":true

	},
	{
		"name":"Apple",
		"price":15,
		"stock":10,
		"isActive":true

	},
	{
		"name":"Mango",
		"price":8,
		"stock":100,
		"isActive":true

	}
];

let loggedUser ;

//Express has methods to use as routes corresponding to each HTTP method.
//get(<endpoint>,<functionToHandle request and responses>)
app.get('/',(request,response)=>{

	//Once the route is accessed, we can send a response with the use of response.send()
	//res.send() actually combines writeHead() and end() already.
	//It is used to send a response to the client and is the end for the response
	response.send("Hello World!")
})

app.get('/hello',(request,response)=>{

	response.send("Hello from Batch 123!")
})

app.post('/',(request,response)=>{

	//request.body will contain the body of a request.
	console.log(request.body);
	response.send(`Hello, I'm ${request.body.name}. I am ${request.body.age}. I could be described as ${request.body.description}.`)
})
//register
app.post('/users',(request,response)=>{

	//since 2 application are communicating with one another, being our client and our api, it is a good practice to always console log the incoming data first.
	console.log(request.body)

	let newUser = {

		email:request.body.email,
		username: request.body.username,
		password: request.body.password,
		isAdmin: request.body.isAdmin

	}

	users.push(newUser)
	console.log(users)
	response.send("Registered Successfully.")

})
//login

app.post('/users/login',(request,response)=>{

	//should contain username and password
	console.log(request.body);

	//find the user with the same username and password from our request body.
	let foundUser = users.find((user)=>{

		return user.username === request.body.username && user.password === request.body.password
	})



	if(foundUser !== undefined){
		/*get the index number of the foundUser, but since the users array is an array of objects we have to use findIndex() instead, will iterate over all of the items
		and return the index number of the current item that matches the return condition. It is similar to find() but instead returns only the index number.*/
	
		let foundUserIndex = users.findIndex((user)=>{

		return user.username === foundUser.username
	})
		foundUser.index = foundUserIndex
		//Temporarily log our user in. allows us to refer the details of a logged in user.
		loggedUser = foundUser
		console.log(loggedUser)
		response.send('Thank you for logging in.')
} else {
	response.send('Login Failed. Wrong Credentials')
}
})

//addItem
app.post('/items',(request,response)=>{

	console.log(loggedUser)
	console.log(request.body)

	let newItem = {

		name: request.body.name,
		description : request.body.description,
		price: request.body.price,
		isActive:request.body.isActive

	}
	if(loggedUser.isAdmin === true ){
		items.push(newItem)
		console.log(items)
		response.send("You have added a new item")

	} else {

		loggedUser = foundUser;
	response.send("Unauthorized: Action Forbidden")
}

})

app.get('/items',(request,response)=>{
	console.log(loggedUser)
	if (loggedUser.isAdmin === true) {
		response.send(items)
	} else {

	} response.send("Unauthorized: Action Forbidden")
})

//getSingleUser
	//GET request should not have a request body. It may have headers for additional information or we can add a small amount of data somewhere else: the url.
	//Route params are values we can pass via the URL.
	//This is done especially to allow us to send small amount of data into our server.
	//Route parameters can be defined in the endpoint of a route with :parameter
app.get('/users/:index',(request,response)=>{

	//request.params is an objects that contains the route params.
	//Its properties are then determined by your route parameters
	console.log(request.params)
	//how do we access the actual route params?
	console.log(request.params.index)
	//request.params.index being a part of the url string is a string, so we need to parse it as property int.
	let index = parseInt(request.params.index)
	/*console.log(typeof index);*/

	let user = users[index]
	response.send(user)

})

//updateUser
	/*We're going to update the password or our user. Howeer we should get the user first. To do this we should not pass the index of the user in the body 
	but instead in our route params.*/

app.put('/users/:index',(request,response)=>{

	console.log(request.params);
	console.log(request.params.index)
	let userIndex = parseInt(request.params.index)

	if(loggedUser !== undefined && loggedUser.index === userIndex){

		//get the proper user from the array with our index:
		//users[userIndex] = obj
		//[userIndex] = number because of parseInt
		//request.body.password comes from the body of your request.

		users[userIndex].password = request.body.password
		console.log(users[userIndex]);
		response.send('User password has been updated.')

	} else {
		response.send('Unauthorized. Login first.')
	}
})

app.get('/items/getSingle/:index',(request,response)=>{
	console.log(request.params.index)
	let itemIndex = parseInt(request.params.index)
	console.log(items[itemIndex])
	response.send(items[itemIndex])

})
app.put('/items/archive/:index',(request,response)=>{

	console.log(request.params.index)
	let itemIndex = parseInt(request.params.index)
	if(loggedUser !== undefined && loggedUser.isAdmin === true){
		items[itemIndex].isActive = false
		response.send('Item Archived.')
	} else {
		response.send('Unauthorized. Action Forbidden')
	}

})
app.put('/items/activate/:index',(request,response)=>{

	console.log(request.params.index)
	let itemIndex = parseInt(request.params.index)
	if(loggedUser !== undefined && loggedUser.isAdmin === true){
		items[itemIndex].isActive = true
		response.send('Item Activated.')
	} else {
		response.send('Unauthorized. Action Forbidden')
	}

})


app.listen(port, ()=>console.log(`Server is running at port ${port}`));
